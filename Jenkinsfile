def pipeline_state = true;
pipeline {

    agent {
        label 'slave001'
    }
/*
    tools {
        maven 'maven-3.6.3'
        jdk 'java-11'
    }
*/
    environment {
        nexus_repo = 'maven-releases'
        nexus_url  = ''
        artifactExtension = '.jar'
    }

    options {
       buildDiscarder(logRotator(numToKeepStr: '30', artifactNumToKeepStr: '10'))
       timestamps()
    }

    stages {
       stage('Build and package') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/master'
                    }
                    expression {
                        pipeline_state == true
                    }
                }
            }
            steps {
                script {
                    try {
                        withSonarQubeEnv('sonar') {
                            sh("""mvn clean install org.sonarsource.scanner.maven:sonar-maven-plugin:sonar \
                            -Dsonar.projectKey=thedevopschef_petclinic-forked -Dsonar.organization=thedevopschef \
                            -Dsonar.host.url=https://sonarcloud.io""")
                        }
                    }
                    catch (Exception e) {
                        currentBuild.result='FAILURE'
                        pipeline_state = false
                    }
                }
            }
        }
        stage("Quality Gate") {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/master'
                    }
                    expression {
                        pipeline_state == true
                    }
                }
            }
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Publish Release Artifact to Nexus') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/master'
                    }
                    expression {
                        pipeline_state == true
                    }
                }
            }
            steps {
                script {
                    try {
                        pomProp   = readMavenPom file: 'pom.xml'
                        nexusPublisher nexusInstanceId: 'nexus3', \
                        nexusRepositoryId: "${nexus_repo}", \
                        packages: [[$class: 'MavenPackage', \
                        mavenAssetList: [[classifier: '', extension: '', \
                        filePath: "target/${pomProp.artifactId}-${pomProp.version}.${pomProp.packaging}"]], \
                        mavenCoordinate: [artifactId: "${pomProp.artifactId}", \
                        groupId: "${pomProp.groupId}", \
                        packaging: "${pomProp.packaging}", \
                        version: "${pomProp.version}-${BUILD_NUMBER}"]]]
                    }
                    catch (Exception e) {
                        pipeline_state = false
                        currentBuild.result='FAILURE'
                        throw e
                        error "Pipeline failed on pushing artifact to Nexus"
                    }
                }
            }
        }
        stage('Publish Test Coverage Report') {
            steps {
                junit '**/target/surefire-reports/*.xml'
                step([$class: 'JacocoPublisher', 
                     execPattern: '**/build/jacoco/*.exec',
                     classPattern: '**/build/classes',
                     sourcePattern: 'src/main/java',
                     exclusionPattern: 'src/test*'
                     ])
            }
        }
        stage('Set Build Status') {
            steps {
                script {
                    filterLogs ('[INFO] BUILD FAILURE', 1)
                    filterLogs ('[ERROR] To see the full stack trace of the errors', 1)
                }
            }
        }
    }
    post {
        always {
            echo "CI for ${env.GIT_BRANCH} Success"
        }
        success {
            echo "CI for ${env.GIT_BRANCH} Success"
        }
        failure {
            echo "CI for ${env.GIT_BRANCH} failed"
        }
        unstable {
            echo "CI for ${env.GIT_BRANCH} is unstable"
        }
        changed {
            echo ''
        }
    }
}

// Custom Method and vars.
import org.apache.commons.lang.StringUtils

def filterLogs(String filter_string, int occurrence) {
    def logs = currentBuild.rawBuild.getLog(10000).join('\n')
    int count = StringUtils.countMatches(logs, filter_string);
    if (count > occurrence - 1) {
        currentBuild.result='FAILURE'
    }
}
